/** @file  Accessors.h
 *  @brief xTRT supplied Accessors header
 *
 *  This file houses a bunch of predefined accessor variables for
 *  you. They are meant to be used with the xTRT::Algorithm::get
 *  function.
 *
 *  @namespace xTRT::Acc
 *  @brief namespace for const xAOD AuxElement accessors
 *
 *  @author Douglas Davis < ddavis@cern.ch >
 */

#ifndef TRTFramework_Accessors_h
#define TRTFramework_Accessors_h
#pragma once

// ATLAS
#include <AthContainers/AuxElement.h>
#include <xAODTracking/TrackStateValidationContainer.h>
#include <xAODTruth/TruthParticleContainer.h>

namespace xTRT {
  namespace Acc {

    /** @addtogroup acc_Event Aux Accessors for Events
     *  @brief SG::AuxElement::ConstAccessor variables for event properties
     *  @{
     */

    using ull = unsigned long long;

    const SG::AuxElement::ConstAccessor<float> TRTOccGlobal  {"TRTOccGlobal"};
    const SG::AuxElement::ConstAccessor<float> TRTOccBarrelA {"TRTOccBarrelA"};
    const SG::AuxElement::ConstAccessor<float> TRTOccBarrelC {"TRTOccBarrelC"};
    const SG::AuxElement::ConstAccessor<float> TRTOccEndcapAA{"TRTOccEndcapAA"};
    const SG::AuxElement::ConstAccessor<float> TRTOccEndcapBA{"TRTOccEndcapBA"};
    const SG::AuxElement::ConstAccessor<float> TRTOccEndcapAC{"TRTOccEndcapAC"};
    const SG::AuxElement::ConstAccessor<float> TRTOccEndcapBC{"TRTOccEndcapBC"};
    const SG::AuxElement::ConstAccessor<float> TrtPhaseTime  {"TrtPhaseTime"};

    /** @}*/

    /** @addtogroup acc_Track Aux Accessors for Tracks
     *  @brief SG::AuxElement::ConstAccessor variables for track properties
     *  @{
     */
    const SG::AuxElement::ConstAccessor<float> eProbabilityToT          {"eProbabilityToT"};
    const SG::AuxElement::ConstAccessor<float> eProbabilityHT           {"eProbabilityHT"};
    const SG::AuxElement::ConstAccessor<float> TRTTrackOccupancy        {"TRTTrackOccupancy"};
    const SG::AuxElement::ConstAccessor<float> ToT_dEdx_noHT_divByL     {"ToT_dEdx_noHT_divByL"};
    const SG::AuxElement::ConstAccessor<float> ToT_usedHits_noHT_divByL {"ToT_usedHits_noHT_divByL"};


    const SG::AuxElement::ConstAccessor<unsigned char> numberOfTRTHits     {"numberOfTRTHits"};
    const SG::AuxElement::ConstAccessor<unsigned char> numberOfTRTOutliers {"numberOfTRTOutliers"};
    const SG::AuxElement::ConstAccessor<unsigned char> numberOfInnermostPixelLayerHits
    {"numberOfInnermostPixelLayerHits"};
    const SG::AuxElement::ConstAccessor<unsigned char> numberOfNextToInnermostPixelLayerHits
    {"numberOfNextToInnermostPixelLayerHits"};
    const SG::AuxElement::ConstAccessor<unsigned char> numberOfPixelHits {"numberOfPixelHits"};
    const SG::AuxElement::ConstAccessor<unsigned char> numberOfSCTHits   {"numberOfSCTHits"};

    const SG::AuxElement::ConstAccessor<float> numberDoF  {"numberDoF"};
    const SG::AuxElement::ConstAccessor<float> chiSquared {"chiSquared"};

    const SG::AuxElement::ConstAccessor<
      ElementLink<xAOD::TruthParticleContainer>
      > truthParticleLink{"truthParticleLink"};

    const SG::AuxElement::ConstAccessor<
      std::vector<ElementLink<xAOD::TrackStateValidationContainer>>
      > msosLink{"msosLink"};

    /** @} */

    /** @addtogroup acc_DCandMSOS Aux Accessors for MSOSs and Drift Circles
     *  @brief SG::AuxElement::ConstAccessor variables for shared between MSOSs and Drift Circles
     *  @{
     */

    // for MSOS and drift Circle
    const SG::AuxElement::ConstAccessor<float> localX {"localX"};
    const SG::AuxElement::ConstAccessor<float> localY {"localY"};

    /** @} */

    /** @addtogroup acc_DriftCircle Aux Accessors for Drift Circles
     *  @brief SG::AuxElement::ConstAccessor variables for drift circle properties
     *  @{
     */

#if __clang__
    const SG::AuxElement::ConstAccessor<ull> identifier {"identifier"};
    const SG::AuxElement::ConstAccessor<std::vector<ull>> rdoIdentifierList{"rdoIdentifierList"};
#else
    const SG::AuxElement::ConstAccessor<unsigned long> identifier {"identifier"};
    const SG::AuxElement::ConstAccessor<std::vector<unsigned long>> rdoIdentifierList{"rdoIdentifierList"};
#endif

    const SG::AuxElement::ConstAccessor<float> localXError{"localXError"};

    const SG::AuxElement::ConstAccessor<float> globalX {"globalX"};
    const SG::AuxElement::ConstAccessor<float> globalY {"globalY"};
    const SG::AuxElement::ConstAccessor<float> globalZ {"globalZ"};

    const SG::AuxElement::ConstAccessor<unsigned int>  bitPattern    {"bitPattern"};
    const SG::AuxElement::ConstAccessor<char>          gasType       {"gasType"};
    const SG::AuxElement::ConstAccessor<int>           bec           {"bec"};
    const SG::AuxElement::ConstAccessor<int>           phi_module    {"phi_modue"};
    const SG::AuxElement::ConstAccessor<int>           layer         {"layer"};
    const SG::AuxElement::ConstAccessor<int>           strawlayer    {"strawlayer"};
    const SG::AuxElement::ConstAccessor<int>           strawnumber   {"strawnumber"};
    const SG::AuxElement::ConstAccessor<int>           TRTboard      {"TRTboard"};
    const SG::AuxElement::ConstAccessor<int>           TRTchip       {"TRTchip"};
    const SG::AuxElement::ConstAccessor<float>         drifttime     {"drifttime"};
    const SG::AuxElement::ConstAccessor<int>           status        {"status"};
    const SG::AuxElement::ConstAccessor<float>         tot           {"tot"};
    const SG::AuxElement::ConstAccessor<char>          isHT          {"isHT"};
    const SG::AuxElement::ConstAccessor<char>          highThreshold {"highThreshold"};
    const SG::AuxElement::ConstAccessor<float>         T0            {"T0"};
    const SG::AuxElement::ConstAccessor<float>         leadingEdge   {"leadingEdge"};

    const SG::AuxElement::ConstAccessor<float> driftTimeToTCorrection{"driftTimeToTCorrection"};
    const SG::AuxElement::ConstAccessor<float> driftTimeHTCorrection {"driftTimeHTCorrection"};

    /** @} */

    /** @addtogroup acc_MSOS Aux Accessors for MSOSs
     *  @brief SG::AuxElement::ConstAccessor variables for msos properties
     *  @{
     */

    const SG::AuxElement::ConstAccessor<int>           type         {"type"};

#if __clang__
    const SG::AuxElement::ConstAccessor<ull>           detElementId {"detElementId"};
#else
    const SG::AuxElement::ConstAccessor<unsigned long> detElementId {"detElementId"};
#endif

    const SG::AuxElement::ConstAccessor<float>         localTheta   {"localTheta"};
    const SG::AuxElement::ConstAccessor<float>         localPhi     {"localPhi"};
    const SG::AuxElement::ConstAccessor<float>         HitZ         {"HitZ"};
    const SG::AuxElement::ConstAccessor<float>         HitR         {"HitR"};
    const SG::AuxElement::ConstAccessor<float>         rTrkWire     {"rTrkWire"};

    const SG::AuxElement::ConstAccessor<float> unbiasedResidualX{"unbiasedResidualX"};
    const SG::AuxElement::ConstAccessor<float> biasedResidualX  {"biasedResidualX"};
    const SG::AuxElement::ConstAccessor<float> unbiasedPullX    {"unbiasedPullX"};
    const SG::AuxElement::ConstAccessor<float> biasedPullX      {"biasedPullX"};

    /** @} */
  }
}

#endif

/* potential future use */
/*
#define MAKE_ACC_FUNCTION(TYPE,NAME)                                    \
  namespace Acc {                                                       \
    const SG::AuxElement::ConstAccessor<TYPE> _##NAME {#NAME};          \
  }                                                                     \
  inline const TYPE& NAME(const SG::AuxElement* obj) {                  \
    if ( xTRT::Acc::_##NAME.isAvailable(*obj) ) {                       \
      return xTRT::Acc::_##NAME(*obj);                                  \
    }                                                                   \
    else {                                                              \
      std::string msg{#NAME}; msg.append(" Unavailable! Exiting!");     \
      std::puts(msg.c_str());                                           \
      std::exit(EXIT_FAILURE);                                          \
    }                                                                   \
  }

namespace xTRT {
  MAKE_ACC_FUNCTION(float, TRTOccGlobal);
  MAKE_ACC_FUNCTION(float, TRTOccBarrelA);
  MAKE_ACC_FUNCTION(float, TRTOccBarrelC);
  MAKE_ACC_FUNCTION(float, TRTOccEndcapAA);
  MAKE_ACC_FUNCTION(float, TRTOccEndcapBA);
  MAKE_ACC_FUNCTION(float, TRTOccEndcapAC);
  MAKE_ACC_FUNCTION(float, TRTOccEndcapBC);
  MAKE_ACC_FUNCTION(float, TrtPhaseTime);

  MAKE_ACC_FUNCTION(float, eProbabilityToT);
  MAKE_ACC_FUNCTION(float, eProbabilityHT);
  MAKE_ACC_FUNCTION(float, TRTTrackOccupancy);
  MAKE_ACC_FUNCTION(float, ToT_dEdx_noHT_divByL);
  MAKE_ACC_FUNCTION(float, ToT_usedHits_noHT_divByL);

  MAKE_ACC_FUNCTION(unsigned char, numberOfTRTHits);
  MAKE_ACC_FUNCTION(unsigned char, numberOfTRTOutliers);
  MAKE_ACC_FUNCTION(unsigned char, numberOfInnermostPixelLayerHits);
  MAKE_ACC_FUNCTION(unsigned char, numberOfNextToInnermostPixelLayerHits);
  MAKE_ACC_FUNCTION(unsigned char, numberOfPixelHits);
  MAKE_ACC_FUNCTION(unsigned char, numberOfSCTHits);

  MAKE_ACC_FUNCTION(float,numberDoF);
  MAKE_ACC_FUNCTION(float,chiSquared);

  MAKE_ACC_FUNCTION(ElementLink<xAOD::TruthParticleContainer>, truthParticleLink);
  MAKE_ACC_FUNCTION(std::vector<ElementLink<xAOD::TrackStateValidationContainer>>, msosLink);
}
*/
