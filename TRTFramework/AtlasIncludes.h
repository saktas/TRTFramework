/** @file  AtlasIncludes.h
 *  @brief TRTFramework ATLAS includes in a single file
 *
 *  @author Douglas Davis < ddavis@cern.ch >
 */

#ifndef TRTFramework_AtlasIncludes_h
#define TRTFramework_AtlasIncludes_h

#if __clang__
#pragma clang diagnostic ignored "-Wtautological-undefined-compare"
#endif

#include <xAODTracking/TrackParticle.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODCore/ShallowCopy.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODMuon/MuonAuxContainer.h>
#include <xAODEgamma/EgammaxAODHelpers.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEgamma/ElectronAuxContainer.h>
#include <xAODTruth/TruthParticleAuxContainer.h>
#include <xAODTruth/TruthVertex.h>
#include <xAODTruth/xAODTruthHelpers.h>
#include <xAODTracking/VertexContainer.h>
#include <xAODTracking/TrackMeasurementValidationContainer.h>
#include <xAODTracking/TrackStateValidationContainer.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>

#include <EventLoop/Algorithm.h>
#include <EventLoop/Worker.h>
#include <AsgTools/AnaToolHandle.h>
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgAnalysisInterfaces/IPileupReweightingTool.h>
#include <TrigConfxAOD/xAODConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include <TriggerMatchingTool/MatchingTool.h>
#include <PathResolver/PathResolver.h>
#include <InDetTrackSelectionTool/InDetTrackSelectionTool.h>
#include <MCTruthClassifier/MCTruthClassifierDefs.h>

namespace xTRT {
  using MSOS        = xAOD::TrackStateValidation;
  using DriftCircle = xAOD::TrackMeasurementValidation;
  //  typedef xAOD::TrackStateValidation MSOS;
  //  typedef xAOD::TrackMeasurementValidation DriftCircle;
}

#endif
