# TRTFramework Doxygen Documentation

If you haven't taken a look at the tutorial for getting started with
TRTFramework, [click here](https://atlastrt.gitbooks.io/trtframework/)

This is the doxygen documentation for TRTFramework. All the links in the
top bar should be pretty self explanatory, though the best place to
start is probably either looking at Classes or looking at Modules.
