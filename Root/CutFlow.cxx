
// System include(s):
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <stdexcept>

// Local include(s):
#include "TRTFramework/CutFlow.h"

namespace xTRT {

  CutFlow::CutFlow( const std::string& name, const initcuts_t& initialCuts )
    : m_name( name ) {

    for( const auto& step : initialCuts ) {
      m_cutflow.push_back( step.second );
      m_stepNames.push_back( step.first );
    }
  }

  std::size_t CutFlow::addStep( const std::string& name ) {

    // Make sure that a step with this name doesn't exist yet:
    if( std::find( m_stepNames.begin(), m_stepNames.end(), name ) !=
        m_stepNames.end() ) {
      throw std::runtime_error( "Step name \"" + name + "\" already defined "
                                "in cut flow: " + m_name );
    }

    // Add this as a new step:
    m_cutflow.push_back( 0 );
    m_stepNames.push_back( name );

    // Return the index of the new step:
    return ( m_stepNames.size() - 1 );
  }

  unsigned int CutFlow::incrementStep( std::size_t step ) {

    checkStepIsValid( step );
    return ++( m_cutflow[ step ] );
  }

  unsigned int CutFlow::incrementStep( std::size_t step, unsigned int amount ) {

    checkStepIsValid( step );
    return ( m_cutflow[ step ] += amount );
  }

  void CutFlow::setName( const std::string& name ) {

    m_name = name;
    return;
  }

  const std::string& CutFlow::name() const {

    return m_name;
  }

  std::size_t CutFlow::numberOfSteps() const {

    return m_stepNames.size();
  }

  std::size_t CutFlow::stepIndex( const std::string& name ) const {

    for( std::size_t i = 0; i < m_stepNames.size(); ++i ) {
      if( m_stepNames[ i ] == name ) {
        return i;
      }
    }

    throw std::runtime_error( "Step name \"" + name + "\" not found in "
                              "cutflow: " + m_name );
  }

  const std::string& CutFlow::stepName( std::size_t step ) const {

    checkStepIsValid( step );
    return m_stepNames[ step ];
  }

  unsigned int CutFlow::stepValue( const std::size_t step ) const {

    checkStepIsValid( step );
    return m_cutflow[ step ];
  }

  unsigned int CutFlow::stepValue( const std::string& name ) const {

    return stepValue( stepIndex( name ) );
  }

  void CutFlow::checkStepIsValid( std::size_t step ) const {

    if( step >= m_stepNames.size() ) {
      throw std::runtime_error( "Step number " + std::to_string( step ) +
                                " not defined in cutflow: " + m_name );
    }
  }

} // namespace xTRT

namespace {

  /// Helper class for resetting the ios flags on a stream object
  class IosFlagReset {

  public:
    /// Constructor, capturing the stream's flags
    IosFlagReset( std::ios& stream )
      : m_stream( stream ), m_flags( stream.flags() ) {}
    /// Destructor, restoring the stream's flags
    ~IosFlagReset() {
      m_stream.setf( m_flags );
    }

  private:
    /// The stream that this object is guarding
    std::ios& m_stream;
    /// The original flags of the stream, that need to be reset on it
    std::ios::fmtflags m_flags;

  }; // class IosFlagReset

} // private namespace

namespace std {

  std::ostream& operator<< ( std::ostream& out, const xTRT::CutFlow& cf ) {

    // Make sure the stream's flags are returned to their original state
    // at the end:
    const IosFlagReset flagGuard( out );

    // Find the longest step name:
    std::size_t stepLength = 0;
    for( std::size_t i = 0; i < cf.numberOfSteps(); ++i ) {
      if( cf.stepName( i ).length() + 2 > stepLength ) {
        stepLength = cf.stepName( i ).length() + 2;
      }
    }

    // Decide about the printout dimensions:
    const std::size_t stepWidth = std::max( stepLength,
                                       static_cast< std::size_t >( 25 ) );
    static const std::size_t countWidth = 15;
    const std::size_t fullWidth = stepWidth + countWidth + 5;

    // Print the payload of the cutflow:
    out << std::setw( fullWidth ) << std::setfill( '=' ) << "=\n";
    out << " CutFlow: " << cf.name() << "\n";
    out << std::setw( fullWidth ) << std::setfill( '-' ) << "-\n";
    out << std::setw( stepWidth ) << std::setfill( ' ' )
        << std::setiosflags( std::ios::right )
        << "Cut Step" << " | " << std::setw( countWidth )
        << std::setiosflags( std::ios::right ) << "Count" << "\n";
    out << std::setw( fullWidth ) << std::setfill( '-' ) << "-\n";
    for( std::size_t i = 0; i < cf.numberOfSteps(); ++i ) {
      out << std::setw( stepWidth ) << std::setiosflags( std::ios::right )
          << std::setfill( ' ' )
          << cf.stepName( i ) << " : " << std::setw( countWidth )
          << std::setiosflags( std::ios::right ) << cf.stepValue( i )
          << "\n";
    }
    out << std::setw( fullWidth - 1 ) << std::setfill( '=' ) << "=";

    // Return the stream:
    return out;
  }

} // namespace std
