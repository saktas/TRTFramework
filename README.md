# TRTFramework

A framework for TRT analysis projects using the ATLAS Run 2 Data Model
(for analyzing (D)(x)AOD's).

To use this repository (which is the framework), add it as a submodule
to your existing git repository.


A tutorial/walkthrough for getting started can be found at [this
gitbook!](https://atlastrt.gitbooks.io/trtframework/)


API documentation can be found
[here](http://phy.duke.edu/~ddavis/TRTFramework/)


Instructions for TRTFramework version 1 can be found here:
[TRTFramework
wiki](https://gitlab.cern.ch/atlas-trt-software/TRTFramework/wikis/home)
